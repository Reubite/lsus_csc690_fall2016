package com.memecreator.reubenajayi.memecreator;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


public class HomeActivity extends AppCompatActivity {

    ListView list;
    String[] meme = {
            "Soccer Meme 1",
            "Soccer Meme 2",
            "Soccer Meme 3",
            "Soccer Meme 4",
            "Soccer Meme 5",
            "Soccer Meme 6",
            "Soccer Meme 7",
            "Soccer Meme 8",
            "Soccer Meme 9"
    };


//    Integer[] imageId = {   R.drawable.meme1,
//                            R.drawable.meme2,
//                            R.drawable.meme3,
//                            R.drawable.meme4,
//                            R.drawable.meme5,
//                            R.drawable.meme6,
//                            R.drawable.meme7,
//                            R.drawable.meme8,
//                            R.drawable.meme9
//                        };

    ArrayList<Integer> imageId = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        imageId.add(R.drawable.meme1);
        imageId.add(R.drawable.meme2);
        imageId.add(R.drawable.meme3);
        imageId.add(R.drawable.meme4);
        imageId.add(R.drawable.meme5);
        imageId.add(R.drawable.meme6);
        imageId.add(R.drawable.meme7);
        imageId.add(R.drawable.meme8);
        imageId.add(R.drawable.meme9);

        Listview adapter = new Listview(HomeActivity.this, meme, imageId);
        list = (ListView) findViewById(R.id.list);
        list.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        addListenerOnButton();

    }

    public void addListenerOnButton() {

        Button createButton = (Button) findViewById(R.id.create);
        createButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                Intent intent = new Intent(HomeActivity.this, URLInputActivity.class);
                startActivity(intent);
                finish();
            }
        });

//        Button viewButton =  (Button)findViewById(R.id.view_button);
//        viewButton.setOnClickListener(new Button.OnClickListener(){
//            @Override
//            public void onClick(View args2){
//                Toast.makeText(HomeActivity.this, "You clicked at ",
//                        Toast.LENGTH_SHORT).show();
////                Intent intent = new Intent(HomeActivity.this, ListviewActivity.class);
////                    startActivity(intent);
////                    finish();
//            }
//        });
    }

    public void addToList(int id){
        imageId.add(id);
    }
}