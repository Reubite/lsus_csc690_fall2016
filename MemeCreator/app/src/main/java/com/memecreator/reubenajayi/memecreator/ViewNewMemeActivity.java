package com.memecreator.reubenajayi.memecreator;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * Created by r09003799 on 10/9/2016.
 */
public class ViewNewMemeActivity extends AppCompatActivity{

    private EditText editText;
    private EditText editText2;
    private File file;
    Bitmap bmp; // defined Bitmap
    Bitmap bmp2;
    ImageView imgShowOuput;
    String path;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upper_lower_input);
        final ImageView test = (ImageView) findViewById(R.id.urlImage);

        file = new File(Environment.getExternalStorageDirectory() + File.separator + "saveimage");
        file.mkdirs();
        editText = (EditText) findViewById(R.id.upper_edit);
        editText2 = (EditText) findViewById(R.id.lower_edit);
        imgShowOuput = (ImageView) findViewById(R.id.urlImage);

        Intent intent = getIntent();
        String url_string = intent.getStringExtra("URL");

        Picasso.with(this)
                .load(url_string)
                .into(test);
        Button viewPicButton = (Button) findViewById(R.id.viewPicButton);
        viewPicButton.setOnClickListener(new View.OnClickListener() { //Saves the UpperEditText
            @Override
            public void onClick(View v) {
                Toast.makeText(ViewNewMemeActivity.this, "Saving Image...", Toast.LENGTH_SHORT).show();
                //HomeActivity memeList = new HomeActivity();
               // memeList.addToList(R.id.urlImage);

                bmp = getBitmapFromView(editText);
                bmp2 = getBitmapFromView(editText2);
                String path = file.getAbsolutePath() + File.separator + System.currentTimeMillis() + "_image" + ".jpg";
                saveBitmapToFile(bmp, path, 100);
                saveBitmapToFile(bmp2, path, 100);
                test.setImageBitmap(bmp);
                test.setImageBitmap(bmp2);


                bmp = writeTextOnImage(ViewNewMemeActivity.this, editText.getText().toString());
                path = file.getAbsolutePath() + File.separator + System.currentTimeMillis() + "_drawImage" + ".jpg";
                saveBitmapToFile(bmp, path, 100);
                test.setImageBitmap(bmp);
                Toast.makeText(ViewNewMemeActivity.this, "Image Saved!!", Toast.LENGTH_SHORT).show();

//                Intent intent = new Intent(ViewNewMemeActivity.this, testNewMemeImage.class);
//                startActivity(intent);
            }
        });
    }

    public static Bitmap getBitmapFromView(View view) {
        view.clearFocus();
        view.setPressed(false);
        view.setFocusable(false);
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }

    public static String saveBitmapToFile(
            Bitmap bitmap, String path, int quality) {
        File imageFile = new File(path);
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e("BitmapToTempFile", "Error writing bitmap", e);
        }
        return imageFile.getAbsolutePath();
    }

    public Bitmap writeTextOnImage(Context mContext, String mText) {
        try {
            Resources resources = mContext.getResources();
            float scale = resources.getDisplayMetrics().density;
            /// Here you need to give your default image
            Bitmap bitmap = BitmapFactory.decodeResource(resources, R.id.urlImage);
            android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
            if (bitmapConfig == null) {
                bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
            }
            bitmap = bitmap.copy(bitmapConfig, true);
            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            // Change Text Size & Color based on your requirement
            paint.setColor(Color.BLACK);
            paint.setTextSize((int) (21 * scale));
            paint.setShadowLayer(20f, 0, 0, Color.LTGRAY);
            Rect bounds = new Rect();
            paint.getTextBounds(mText, 0, mText.length(), bounds);
            // Change position based on your requirement
            int x = 20;
            int y = 20;
            canvas.drawText(mText, x * scale, y * scale, paint);
            return bitmap;
        } catch (Exception e) {
            return null;
        }
    }
}
