package com.lightbulb.reubenajayi.lightbulb;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.app.Activity;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.util.Log;
import android.hardware.camera2.params.*;
import android.hardware.Camera;

import java.security.Policy;


public class MainActivity extends AppCompatActivity {

    private Button buttonOn;
    private Button buttonOff;
    private Button bulbChange;
    private ImageView image;
    private CameraManager mCamera;
    private Boolean isFlashOn;
    private String mCameraId;
    private Camera.Parameters mparameters;
    private Camera mcamera;

    //@TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MainActivity", "OnCreate()" + this.toString());
        setContentView(R.layout.activity_main);
        addListenerOnButton();
    }


    public void addListenerOnButton() {
        image = (ImageView) findViewById(R.id.img);

        buttonOn = (Button) findViewById(R.id.bulb_on);
        buttonOff = (Button) findViewById(R.id.bulb_off);
        isFlashOn = false;
        Boolean flashIsSupported = getApplicationContext().
                getPackageManager().
                hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        //Check if phone supports flash or not
        if (!flashIsSupported) {
            AlertDialog notification = new AlertDialog.Builder(MainActivity.this).create();
            notification.setTitle("Blunder!");
            notification.setMessage("Sorry, your device does not support flash light.");
            notification.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish(); //Close application
                    System.exit(0);
                }
            });
            notification.show();
            return;
        }


        mCamera = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            mCameraId = mCamera.getCameraIdList()[0];
        } catch (Exception e) {
            e.printStackTrace();
        }

        buttonOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                try {
                        image.setImageResource(R.drawable.bulb_on);
                        switchFlashOn();
                        isFlashOn = true;
                    }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        buttonOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg1) {
                try {
                    if (isFlashOn) {
                        image.setImageResource(R.drawable.bulb_off);
                        switchFlashOff();
                        isFlashOn = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void switchFlashOn() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mCamera.setTorchMode(mCameraId, true);
            }
            else{
                mcamera = Camera.open();
                Camera.Parameters p = mcamera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                mcamera.setParameters(p);
                mcamera.stopPreview();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void switchFlashOff() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                mCamera.setTorchMode(mCameraId, false);
            }
            else {
                Camera.Parameters p = mcamera.getParameters();
                p.setFlashMode(p.FLASH_MODE_OFF);
                mcamera.setParameters(p);
                mcamera.release();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
}